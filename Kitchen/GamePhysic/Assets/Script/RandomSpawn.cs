﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawn : MonoBehaviour
{
    public GameObject prefab1;

    public float spawnRate = 2f;

    float nextSpawn = 0f;

    int whatToSpawn;

    Vector3 _rotateCar;
    // Start is called before the first frame update
    void Start()
    {
        //transform.Rotate(0, -90, 0);
        //transform.Rotate(0, -90, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextSpawn)
        {
            Instantiate(prefab1, transform.position, Quaternion.identity);    
            nextSpawn = Time.time + spawnRate;
        }
    }

    
}
