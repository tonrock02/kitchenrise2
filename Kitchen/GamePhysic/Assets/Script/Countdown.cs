﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour
{
    public static Countdown instance;
    [SerializeField]
    public float CurrentTime;
    public float StartingTime;
    public Text CountdownText;
    void Start()
    {
        if (instance == null)
            instance = this;
    CurrentTime = StartingTime;
    }

    // Update is called once per frame
    void Update()
    {
        CurrentTime -= 1 * Time.deltaTime;
        CountdownText.text=CurrentTime.ToString("0.0");
        if (CurrentTime <= 0)
        {
            CurrentTime = 0;
        }
    }
}
