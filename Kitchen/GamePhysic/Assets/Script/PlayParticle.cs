﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayParticle : MonoBehaviour
{
    [SerializeField] ParticleSystem Particle1;
    
  


    public void Playparticle()
    {
        Particle1.Play();
     
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Playparticle();
        }
    }
}
